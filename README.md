WST - D branch: libxdg-support
===========================

This module is meant to act as a tiny helper for dealing
with the xdg specification for per-user configuration and
data storage.

The base of this was ripped directly from my
[DImCha project](https://gitlab.com/Wazubaba/dimcha-dport)'s
`util.d` file, and will be maintained seperately from dimcha.

There are presently two functions.
All functions have inline documentation, buildable via
`dub build -b docs`.

