module wst.xdg;

/// Fetches the proper location to store data.
string getDataPath()
{
	import std.process: environment;
	import std.string: format;

	string basePath;
	debug
	{
		basePath = "testcfg";
	}
	else
	{
		version(Windows) basePath = environment["%APPDATA%"];
		version(linux)
		{
			string dataDir;
			if ((dataDir = environment.get("XDG_DATA_DIRS")) !is null)
			{
				import std.string: split;
				basePath = dataDir.split(":")[0];
			}
			else
			{
				import std.string: format;
				basePath = environment.get("XDG_DATA_HOME", "%s/.local/share".format(environment.get("HOME")));
			}
		}
	}
	return basePath;
}

/// Fetches the proper location to store configuration.
string getConfigPath()
{
	import std.process: environment;
	import std.string: format;
	string basePath;
	debug
	{
		basePath = "testcfg";
	}
	else
	{
		version(Windows) basePath = environment["%APPDATA%"];
		version(linux)
		{
			string dataDir;
			if ((dataDir = environment.get("XDG_CONFIG_DIRS")) !is null)
			{
				import std.string: split;
				basePath = dataDir.split(":")[0];
			}
			else
			{
				import std.string: format;
				basePath = environment.get("XDG_CONFIG_HOME", "%s/.config".format(environment.get("HOME")));
			}
		}
	}
	return basePath;
}

